# Lazy Loading To Avoid Pagination vanilla JS

- Webpage link: https://lazy-loading-to-avoid-pagination-vanilla-js-abir-7f8b1a99659952.gitlab.io/

### Lazy loading 
This code implements lazy loading of images, where more images are loaded as the user scrolls down the page to provide a smoother user experience and improve page loading time. Images are fetched from the Picsum Photos API using JavaScript's fetch, and they are displayed in a responsive layout.

- 1) <title>Lazy Loading Images Example</title>: Sets the title of the webpage that appears in the browser's title bar or tab.

- 2) &lt;style&gt;: This is where CSS styles for the HTML document are defined.

- 3) &lt;body&gt;: The body element represents the content of the HTML document.

- 4) #image-container: A div with the id image-container where the images will be loaded.

- 5) .image-card: CSS class for styling the image cards.

- 6) .image-card img: CSS for styling the images inside the image cards.

- 7) &lt;script&gt;: This is where JavaScript code is placed.

### JavaScript code
- Defines variables like page, perPage, and loading to manage image loading.

- Defines functions to fetch images (fetchImages), render images (renderImages), and load more images (loadMoreImages).

- Sets up an event listener for the scroll event to trigger loading more images as the user scrolls down.

- Calls loadMoreImages initially to load the first set of images.

[![Lazy Loading to Avoid Pagination vanilla JS](https://img.youtube.com/vi/1uXR7ao942A/0.jpg)](https://www.youtube.com/watch?v=1uXR7ao942A)
